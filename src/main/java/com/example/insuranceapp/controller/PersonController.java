package com.example.insuranceapp.controller;

import com.example.insuranceapp.model.Insurance;
import com.example.insuranceapp.model.Person;
import com.example.insuranceapp.service.InsuranceService;
import com.example.insuranceapp.service.PersonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

@Controller
@RequestMapping("/api/insuranceapp")
public class PersonController {

    private final PersonService personService;
    private final InsuranceService insuranceService;

    public PersonController(PersonService personService, InsuranceService insuranceService) {
        this.personService = personService;
        this.insuranceService = insuranceService;
    }

    @GetMapping()
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView("index");
        return mav;
    }

    @GetMapping("/persons")
    public ModelAndView getAllPersons() {
        ModelAndView mav = new ModelAndView("persons");
        mav.addObject("persons", personService.getPersons());
        return mav;
    }

    @GetMapping("/insurances")
    public ModelAndView getAllInsurances() {
        ModelAndView mav = new ModelAndView("insurances");
        mav.addObject("insurances", insuranceService.getInsurances());
        return mav;
    }

    @GetMapping("/addPersonForm")
    public ModelAndView addPersonForm() {
        ModelAndView mav = new ModelAndView("add-person");
        Person person = new Person();
        mav.addObject("person", person);
        return mav;
    }

    @PostMapping("/savePerson")
    public String savePerson(@ModelAttribute Person person) {
        personService.savePerson(person);
        return "redirect:/api/insuranceapp/persons";
    }

    @PostMapping("/savePersonDetail")
    public String savePersonDetail(@RequestParam Long personId, @ModelAttribute Person person) {
        personService.savePerson(person);
        return "redirect:/api/insuranceapp/personInsurances?personId=" + personId;
    }

    @GetMapping("/editPersonForm")
    public ModelAndView editPersonForm(@RequestParam Long personId) {
        ModelAndView mav = new ModelAndView("add-person");
        mav.addObject("person", personService.getPerson(personId));
        return mav;
    }

    @GetMapping("/editPersonFormDetail")
    public ModelAndView editPersonFormDetail(@RequestParam Long personId) {
        ModelAndView mav = new ModelAndView("edit-person-detail");
        mav.addObject("person", personService.getPerson(personId));
        return mav;
    }

    @GetMapping("deletePerson")
    public String deletePerson(@RequestParam Long personId) {
        personService.deletePerson(personId);
        return "redirect:/api/insuranceapp/persons";
    }

    @GetMapping("personInsurances")
    public ModelAndView personDetailAndInsurances(@RequestParam Long personId) {
        ModelAndView mav = new ModelAndView("person-detail");
        mav.addObject("person", personService.getPerson(personId));
        mav.addObject("insurances", insuranceService.getInsurancesOfPerson(personId));
        return mav;
    }

    @GetMapping("/addInsuranceForm")
    public ModelAndView addInsuranceForm(@RequestParam Long personId) {
        ModelAndView mav = new ModelAndView("add-insurance");
        Insurance insurance = new Insurance();
        mav.addObject("insurance", insurance);
        mav.addObject("person", personService.getPerson(personId));
        return mav;
    }

    @GetMapping("/editInsuranceForm")
    public ModelAndView editInsuranceForm(@RequestParam Long personId, @RequestParam Long insuranceId) {
        ModelAndView mav = new ModelAndView("edit-insurance");
        mav.addObject("insurance", insuranceService.getInsurance(insuranceId));
        mav.addObject("person", personService.getPerson(personId));
        return mav;
    }

    @PostMapping("/saveInsurance")
    public String saveInsurance(@RequestParam Long personId, @ModelAttribute Insurance insurance) {
        personService.saveInsurance(personId, insurance);
        return "redirect:/api/insuranceapp/personInsurances?personId=" + personId;
    }

    @PostMapping("/saveEditedInsurance")
    public String saveEditedInsurance(@RequestParam Long personId, @ModelAttribute Insurance insurance) {
        personService.saveEditedInsurance(personId, insurance);
        return "redirect:/api/insuranceapp/personInsurances?personId=" + personId;
    }

    @GetMapping("deleteInsurance")
    public String deleteInsurance(@RequestParam Long personId, @RequestParam Long insuranceId) {
        insuranceService.deleteInsurance(insuranceId);
        return "redirect:/api/insuranceapp/personInsurances?personId=" + personId;
    }

    @GetMapping("insuranceDetail")
    public ModelAndView insuranceDetail(@RequestParam Long personId, @RequestParam Long insuranceId) {
        ModelAndView mav = new ModelAndView("insurance-detail");
        mav.addObject("person", personService.getPerson(personId));
        mav.addObject("insurance", insuranceService.getInsurance(insuranceId));
        return mav;
    }
}
