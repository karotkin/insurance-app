package com.example.insuranceapp.model;

public enum InsuranceType {
    CAR("Pojištění auta"), ASSETS("Pojištění majetku"), RESPONSIBILITY("Pojištění odpovědnosti");

    private final String displayValue;

    private InsuranceType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
