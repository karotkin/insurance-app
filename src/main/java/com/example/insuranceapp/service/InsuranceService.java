package com.example.insuranceapp.service;

import com.example.insuranceapp.model.Insurance;
import com.example.insuranceapp.repository.InsuranceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsuranceService {

    private final InsuranceRepository insuranceRepository;

    public InsuranceService(InsuranceRepository insuranceRepository) {
        this.insuranceRepository = insuranceRepository;
    }

    public List<Insurance> getInsurancesOfPerson(Long id) {
        return insuranceRepository.findInsurancesByPersonId(id);
    }

    public Insurance getInsurance(Long id) {
        return insuranceRepository.findById(id).get();
    }

    public void deleteInsurance(Long id) {
        insuranceRepository.deleteById(id);
    }

    public List<Insurance> getInsurances() {
        return insuranceRepository.findAll();
    }
}
