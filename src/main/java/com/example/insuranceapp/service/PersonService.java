package com.example.insuranceapp.service;

import com.example.insuranceapp.model.Insurance;
import com.example.insuranceapp.model.Person;
import com.example.insuranceapp.repository.InsuranceRepository;
import com.example.insuranceapp.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    private final PersonRepository personRepository;
    private final InsuranceRepository insuranceRepository;

    public PersonService(PersonRepository personRepository, InsuranceRepository insuranceRepository) {
        this.personRepository = personRepository;
        this.insuranceRepository = insuranceRepository;
    }

    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    public void savePerson(Person person) {
        personRepository.save(person);
    }

    public Person getPerson(Long id) {
        return personRepository.findById(id).get();
    }

    public void deletePerson(Long id) {
        personRepository.deleteById(id);
    }

    public void saveInsurance(Long id, Insurance insurance) {
        Person person = getPerson(id);
        person.getInsurances().add(insurance);
        savePerson(person);
    }

    public void saveEditedInsurance(Long id, Insurance insurance) {
        Person person = getPerson(id);
        int index = person.getInsurances().indexOf(insuranceRepository.findById(insurance.getId()).get());
        person.getInsurances().set(index, insurance);
        savePerson(person);
    }
}
