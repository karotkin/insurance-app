package com.example.insuranceapp.repository;

import com.example.insuranceapp.model.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {

    @Query("SELECT p.insurances FROM Person p WHERE p.id = :id")
    List<Insurance> findInsurancesByPersonId(Long id);
}
